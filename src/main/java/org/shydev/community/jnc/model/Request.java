/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model;

import java.io.Serializable;
import java.time.Instant;

/**
 * The Request class, this is a very simple object intended to be the foundation of all objects that are sent over
 * the network. It is strongly recommended to extend this class and create your own implementation.
 *
 * @author Shyzus<dev@shyzus.com>
 * @version 1.2
 * @since 1.0
 */
public class Request implements Serializable {

    private Instant timestamp;
    private Object payload;

    /**
     * Default constructor does not set any variables.
     */
    public Request() {
    }

    /**
     * Request constructor
     *
     * @param payload Payload of the request.
     * @param timestamp Timestamp of when the request was formed.
     * @since 1.0
     */
    public Request(Object payload, Instant timestamp) {
        this.timestamp = timestamp;
        this.payload = payload;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Object getPayload() {
        return payload;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
