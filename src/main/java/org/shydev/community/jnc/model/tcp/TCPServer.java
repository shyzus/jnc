/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.tcp;


import org.shydev.community.jnc.model.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCPServer.
 *
 * The TCPServer runs as a single thread that is the entrypoint for new connections. Once a connection is established
 * it is passed into a client handler and the handler is executed within a work-stealing pool.
 *
 * @see Thread
 *
 * @author Shyzus<dev@shyzus.com>
 * @version 1.2
 * @since 1.0
 */
public class TCPServer extends Thread {

    private final int PORT;
    private ServerSocket serverSocket;
    private ConcurrentHashMap<Socket, TCPClientHandler> clients;
    private final ExecutorService POOL_EXECUTOR = Executors.newWorkStealingPool();
    private Class<? extends TCPClientHandler> clientHandlerClass;
    private BiConsumer<ObjectInputStream, ObjectOutputStream> requestHandler;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * TCPServer constructor
     *
     * @param port Port to operate on.
     * @param clientHandlerClass TCPClientHandler implementation class reference
     * @param requestHandler BiConsumer containing the logic to handle the sockets input and output.
     * @since 1.0
     */
    public TCPServer(int port, Class<? extends TCPClientHandler> clientHandlerClass, BiConsumer<ObjectInputStream, ObjectOutputStream> requestHandler) {
        this.PORT = port;
        this.clientHandlerClass = clientHandlerClass;
        this.requestHandler = requestHandler;
        clients = new ConcurrentHashMap<>();
        setName(getClass().getName());
    }


    /**
     * TCPServer run method.
     *
     * This is where the TCPServer receives a new connection and creates a new handler for it, then submits it into the
     * work-stealing pool. It will do so until the ServerSocket is closed.
     *
     * @see ServerSocket
     * @since 1.0
     */
    @Override
    public void run() {

        while (!serverSocket.isClosed()) {
            try {
                Socket socket = serverSocket.accept();
                TCPClientHandler clientHandler = clientHandlerClass.getDeclaredConstructor(Socket.class, BiConsumer.class).newInstance(socket, requestHandler);
                clients.put(socket, clientHandler);
                LOGGER.log(Level.INFO, "Added client with ip: " + socket.getInetAddress().getHostAddress());
                POOL_EXECUTOR.execute(clientHandler);
            } catch (IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    /**
     * Starting point of the TCPServer.
     *
     * This method is responsible for creating the ServerSocket.
     * @since 1.0
     */
    @Override
    public synchronized void start() {
        try {
            serverSocket = new ServerSocket();
            serverSocket.setPerformancePreferences(0,2,1);
            serverSocket.bind(new InetSocketAddress(PORT));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }
        LOGGER.log(Level.INFO, "Started TCPServer on port: " + PORT);
        super.start();
    }

    /**
     * This method closes the ServerSocket but also immediately stops the work-stealing pool from executing.
     *
     * @throws IOException This may be thrown when the socket could not be closed or there was an issue halting the
     * work-stealing pool.
     *
     * @since 1.0
     */
    public void close() throws IOException {
        POOL_EXECUTOR.shutdownNow();
        serverSocket.close();
    }

    public int getPort() {
        return PORT;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public Collection<TCPClientHandler> getClientHandlers() {
        return clients.values();
    }

    /**
     * Gets a client handler if available if the supplied input stream matches.
     *
     * @since 1.2
     * @param objectInputStream input stream to match with.
     * @return TCPClientHandler containing supplied input stream
     */
    public TCPClientHandler getClientHandlerByInputStream(ObjectInputStream objectInputStream) {
        AtomicReference<TCPClientHandler> result = new AtomicReference<>();
        clients.forEach((socket, tcpClientHandler) -> {
            try {
                if (tcpClientHandler.getInputStream().equals(objectInputStream)) {
                    result.set(tcpClientHandler);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        if (result.get() == null) {
            throw new NullPointerException("Could not find client handler with this input stream!");
        } else {
            return result.get();
        }
    }

    /**
     * Gets a client handler if available if the supplied output stream matches.
     *
     * @since 1.2
     * @param objectOutputStream output stream to match with.
     * @return TCPClientHandler containing supplied output stream
     */
    public TCPClientHandler getClientHandlerByOutputStream(ObjectOutputStream objectOutputStream) {
        AtomicReference<TCPClientHandler> result = new AtomicReference<>();
        clients.forEach((socket, tcpClientHandler) -> {
            try {
                if (tcpClientHandler.getOutputStream().equals(objectOutputStream)) {
                    result.set(tcpClientHandler);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        if (result.get() == null) {
            throw new NullPointerException("Could not find client handler with this output stream!");
        } else {
            return result.get();
        }
    }

    public void distributeRequest(Request request) {
        clients.forEach((address, tcpClientHandler) -> tcpClientHandler.sendRequest(request));
    }

}
