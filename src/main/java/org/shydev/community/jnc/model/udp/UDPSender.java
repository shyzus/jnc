/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.udp;

import java.io.*;
import java.net.*;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The UDPClient class represents a single thread object dedicated to processing incoming and outgoing data trough a
 * single DatagramSocket.
 *
 * @see DatagramSocket
 * @see Thread
 *
 * @author Shyzus<dev@shyzus.com>
 * @version 1.2
 * @since 1.0
 */
public class UDPSender extends Thread {

    private final int PORT;
    private int bufferSize = 1024;
    private int timeout = 1000;
    private final DatagramSocket SOCKET;
    private DatagramPacket currentDatagramPacket;
    private InetAddress address;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private BiConsumer<ObjectInputStream, ObjectOutputStream> onReceive = null;

    /**
     * UDPClient constructor
     *
     * @param hostname Hostname to connect with can be IP or domain name recognized by DNS.
     * @param port Port to connect to.
     * @throws SocketException Failed to create a socket, check network permissions.
     * @throws UnknownHostException Failed to identify the given host.
     * @since 1.0
     */
    public UDPSender(String hostname, int port) throws SocketException, UnknownHostException {
        super(UDPSender.class.getName());
        address = InetAddress.getByName(hostname);
        this.PORT = port;
        SOCKET = new DatagramSocket();
        LOGGER.log(Level.INFO, String.format("Created socket at: %s on port: %d", hostname, port));
    }

    /**
     * UDPClient constructor
     *
     * @param port Port to be bound to.
     * @throws SocketException Failed to create a socket, check network permissions.
     * @since 1.0
     */
    public UDPSender(int port) throws SocketException {
        super(UDPSender.class.getName());
        this.PORT = port;
        SOCKET = new DatagramSocket(port);

        LOGGER.log(Level.INFO, String.format("Created socket at port: %d", port));
    }

    /**
     * The overridden run method for the UDPClient. Almost all of the logic is found here this section of code will
     * remain running until the thread is interrupted or a stop condition is met.
     *
     * DO NOT INVOKE DIRECTLY!
     *
     * @since 1.0
     */
    @Override
    public void run() {

        LOGGER.log(Level.INFO, "Starting socket.");
        while (!SOCKET.isClosed()) {
            byte[] buffer = new byte[bufferSize];
            currentDatagramPacket = new DatagramPacket(buffer, buffer.length, address, PORT);
            try {
                SOCKET.receive(currentDatagramPacket);

                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(currentDatagramPacket.getData(), 0, currentDatagramPacket.getLength());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bufferSize);

                try(
                        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)
                ) {

                    onReceive.accept(objectInputStream, objectOutputStream);

                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage());
                }


            } catch (IOException e) {
                LOGGER.log(Level.INFO, e.getMessage());
            }

        }
    }

    /**
     * Pass a consumer for each received Request object.
     *
     * @param consumer Consumer that consumes a ObjectInputStream and ObjectOutputStream
     * @since 1.0
     */
    public void onReceive(BiConsumer<ObjectInputStream, ObjectOutputStream> consumer) {
        onReceive = consumer;
    }

    /**
     * Process a request object into a byte array for transportation.
     *
     * @param request Request object.
     * @throws IOException Something went wrong while converting the request object to bytes.
     *
     * @since 1.0
     */
    private byte[] processOutgoingRequest(Object request) throws IOException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();

        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Send a request to the server.
     *
     * @param request The request object.
     * @throws IOException Something went wrong while converting the request object to bytes or sending the
     * datagramPacket.
     *
     * @since 1.0
     */
    public void sendRequest(Object request) throws IOException {
        byte[] buffer = processOutgoingRequest(request);

        DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length, address, PORT);
        SOCKET.send(datagramPacket);

    }

    /**
     * Send a request to the server.
     *
     * @param request The request object
     * @param port port to use
     * @throws IOException Something went wrong while converting the request object to bytes or sending the
     * datagramPacket.
     *
     * @since 1.0
     */
    public void sendRequest(Object request, InetAddress address, int port) throws IOException {
        byte[] buffer = processOutgoingRequest(request);

        DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length, address, port);
        try {
            SOCKET.send(datagramPacket);
        } catch (SocketException e) {
            LOGGER.log(Level.INFO, "Socket closed with a open channel.");
        }
    }

    /**
     * Closes the socket and joins the thread to the calling thread.
     *
     * @since 1.0
     */
    public void close() {
        SOCKET.close();
        LOGGER.log(Level.INFO, "Closing socket.");
        try {
            join(timeout);
            LOGGER.log(Level.INFO, getName() + " joined.");
        } catch (InterruptedException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Get the timeout configured for joining the thread.
     *
     * @since 1.0
     * @return Timeout in milliseconds.
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Set the timeout for joining the thread.
     *
     * @param timeout Timeout in milliseconds.
     * @since 1.0
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public DatagramPacket getCurrentDatagramPacket() {
        return currentDatagramPacket;
    }
}
