/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.tcp;

import org.shydev.community.jnc.model.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.IllegalBlockingModeException;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCPClient.
 *
 * The TCPClient connects to a specified port and host.
 *
 * @see Thread
 *
 * @author Shyzus<dev@shyzus.com>
 * @version 1.2
 * @since 1.0
 */
public class TCPClient extends Thread {

    private Socket clientSocket;
    private final String HOSTNAME;
    private final int PORT;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private BiConsumer<ObjectInputStream,ObjectOutputStream> requestHandler;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * TCPClient Constructor
     *
     * @param hostname Hostname to connect to.
     * @param port Port to operate on.
     * @param requestHandler Handler for request logic.
     */
    public TCPClient(String hostname, int port, BiConsumer<ObjectInputStream,ObjectOutputStream> requestHandler) {
        this.HOSTNAME = hostname;
        this.PORT = port;
        this.requestHandler = requestHandler;
        setName(getClass().getName());
    }

    /**
     * @since 1.2
     */
    public void connect() throws IOException {
        clientSocket = new Socket();
        clientSocket.setPerformancePreferences(0,2,1);

        try {
            clientSocket.connect(new InetSocketAddress(HOSTNAME, PORT));
        } catch (IllegalBlockingModeException|IllegalArgumentException|SecurityException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }

        out = new ObjectOutputStream(clientSocket.getOutputStream());
        in = new ObjectInputStream(clientSocket.getInputStream());

        LOGGER.log(Level.INFO, "Started TCPClient connecting to: " + HOSTNAME + " on port " + PORT);
    }

    /**
     *
     * Run method of the TCPClient.
     *
     * This method passes the input and output streams to the request handler while the client socket is still connected.
     * @since 1.0
     */
    @Override
    public void run() {
        while (true) {
            if (clientSocket != null && !clientSocket.isClosed() && clientSocket.isConnected() && in != null && out != null) {
                requestHandler.accept(in,out);
            } else if (clientSocket != null && clientSocket.isClosed()) {
                LOGGER.log(Level.INFO,"Detected closed socket no longer handling requests.");
                break;
            }
        }

        try {
            join(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method facilitates the TCPClient to send requests to the server.
     *
     * @param request Request object to send.
     * @since 1.0
     */
    public void sendRequest(Request request) {
        try {
            out.writeObject(request);
            out.flush();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Shutdown the input and output streams of the client socket and closes the TCPClient's socket.
     *
     * @throws IOException This can be thrown if something went wrong with closing the TCPClient's socket or the streams.
     * @since 1.0
     */
    public void close() throws IOException {
        clientSocket.close();
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public String getHostname() {
        return HOSTNAME;
    }

    public int getPort() {
        return PORT;
    }

    public ObjectOutputStream getOut() {
        return out;
    }

    public ObjectInputStream getIn() {
        return in;
    }

    public BiConsumer<ObjectInputStream, ObjectOutputStream> getRequestHandler() {
        return requestHandler;
    }

    public void setRequestHandler(BiConsumer<ObjectInputStream, ObjectOutputStream> requestHandler) {
        this.requestHandler = requestHandler;
    }
}
