/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.tcp;


import org.shydev.community.jnc.model.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCPClientHandler.
 *
 * This class serves as the requires base for any client handling for the JNC TCPServer.
 * This is an abstract class and no instances can be made from it. You will need to extend this class and make your own
 * implementation that has a run method.
 *
 * @see Runnable
 *
 * @author Shyzus<dev@shyzus.com>
 * @version 1.2
 * @since 1.0
 */
public abstract class TCPClientHandler implements Runnable {

    private Socket clientSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private BiConsumer<ObjectInputStream,ObjectOutputStream> requestHandler;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * TCPClientHandler constructor.
     *
     * @param socket socket of the client.
     * @param requestHandler handler to handle any incoming or outgoing requests.
     * @since 1.0
     */
    public TCPClientHandler(Socket socket, BiConsumer<ObjectInputStream,ObjectOutputStream> requestHandler) {
        this.clientSocket = socket;
        this.requestHandler = requestHandler;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public ObjectOutputStream getOutputStream() throws IOException {
        if (out == null) {
            out = new ObjectOutputStream(this.clientSocket.getOutputStream());
        }
        return out;
    }

    public ObjectInputStream getInputStream() throws IOException {
        if (in == null) {
            in = new ObjectInputStream(this.clientSocket.getInputStream());
        }
        return in;
    }

    public void setOut(ObjectOutputStream out) {
        this.out = out;
    }

    public void setIn(ObjectInputStream in) {
        this.in = in;
    }

    public BiConsumer<ObjectInputStream, ObjectOutputStream> getRequestHandler() {
        return requestHandler;
    }

    public void setRequestHandler(BiConsumer<ObjectInputStream, ObjectOutputStream> requestHandler) {
        this.requestHandler = requestHandler;
    }

    public void sendRequest(Request request) {
        try {
            out.writeObject(request);
            out.flush();
            LOGGER.log(Level.INFO, "Client sent: " + request.getPayload().toString());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    public void disconnect() throws IOException {
        clientSocket.close();
    }
}
