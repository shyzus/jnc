/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.internal;

import org.shydev.community.jnc.model.tcp.TCPClientHandler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.function.BiConsumer;

/**
 * Internal TCPClientHandler
 *
 * This class is only meant to be used within the JNC project and not meant to be used by other projects.
 *
 * @see org.shydev.community.jnc.model.tcp.TCPClientHandler
 *
 * @author Shyzus<dev@shyzus.com>
 *
 * @version 1.2
 * @since 1.0
 */
public class TCPClientHandlerImpl extends TCPClientHandler {

    /**
     * TCPClientHandler Constructor
     *
     * @param socket Socket connection between server and client.
     * @param requestHandler BiConsumer that handles the logic involving both input and output streams.
     * @since 1.0
     */
    public TCPClientHandlerImpl(Socket socket, BiConsumer<ObjectInputStream, ObjectOutputStream> requestHandler) {
        super(socket, requestHandler);
    }

    /**
     * Run method
     *
     * The handler maintains a connection with each socket until its closed. While connected anything that is
     * sent or received is handled by the passed BiConsumer.
     *
     * @since 1.0
     */
    @Override
    public void run() {

        while (!getClientSocket().isClosed()) {
            try {
                getRequestHandler().accept(getInputStream(), getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
