/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.udp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.shydev.community.jnc.model.Request;

import java.io.IOException;
import java.net.InetAddress;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EchoTest {

    public UDPSender server;
    public final int COUNT_LIMIT = 10;
    public final int PORT = 65535;
    public final Logger LOGGER = Logger.getLogger(getClass().getName());

    @BeforeEach
    public void setUp() throws Exception {
        server = new UDPSender(PORT);
        server.start();
    }

    @Test
    public void testClientMessageToServer() throws Exception {

        AtomicInteger counter = new AtomicInteger();

        AtomicReference<String> msgToSend = new AtomicReference<>("Test message: " + counter.getAcquire());

        server.onReceive(((objectInputStream, objectOutputStream) -> {
            try {
                Request request = (Request) objectInputStream.readObject();
                Duration latency = Duration.between(request.getTimestamp(), Instant.now());
                LOGGER.log(Level.INFO, "Request received: " + request.getPayload().toString() + "\t latency: " + latency.toMillis() + "ms.");

                assertEquals(request.getPayload().toString(), msgToSend.getAcquire());
                counter.getAndIncrement();

                msgToSend.set("Test message: " + counter.getAcquire());
                Request newRequest = new Request(msgToSend.getAcquire(), Instant.now());
                server.sendRequest(newRequest, InetAddress.getByName("localhost"), PORT);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }));


        server.sendRequest(new Request(msgToSend.getAcquire(), Instant.now()), InetAddress.getByName("localhost"), PORT);

        while (counter.getAcquire() < COUNT_LIMIT) {
            Thread.onSpinWait();
        }
    }

    @AfterEach
    public void tearDown() {
        server.close();
    }
}
