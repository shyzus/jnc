/*
 *     Copyright (C) 2020  Shyzus
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *     Or contact <support@shyzus.com>.
 */

package org.shydev.community.jnc.model.tcp;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.shydev.community.jnc.internal.TCPClientHandlerImpl;
import org.shydev.community.jnc.model.Request;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EchoTest {

    public TCPServer tcpServer;
    public TCPClient tcpClient;

    public final int PORT = 65535;
    public final String HOSTNAME = "localhost";
    public final int COUNT_LIMIT = 10;
    public final Logger LOGGER = Logger.getLogger(getClass().getName());

    @Test
    public void testEcho() {

        AtomicInteger counter = new AtomicInteger(0);

        BiConsumer<ObjectInputStream, ObjectOutputStream> serverHandler = (in, out) -> {
            try {
                Request request = (Request) in.readObject();

                LOGGER.log(Level.INFO, "Server received: " + request.getPayload().toString());
                out.writeObject(request);
                out.flush();
                LOGGER.log(Level.INFO, "Server sent: " + request.getPayload().toString());
            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Casting of request object failed!");
            } catch (EOFException e) {
                LOGGER.log(Level.INFO, "InputStream disconnected");
                try {
                    in.close();
                    out.close();
                } catch (IOException ioException) {
                    LOGGER.log(Level.INFO, "Abrupt closing of streams: " + e.getMessage());
                }
            } catch (IOException e) {
                LOGGER.log(Level.INFO, e.getMessage());
            }
        };

        tcpServer = new TCPServer(PORT, TCPClientHandlerImpl.class, serverHandler);
        tcpServer.start();

        BiConsumer<ObjectInputStream, ObjectOutputStream> clientHandler = (in, out) -> {
            try {
                Request request = (Request) in.readObject();
                LOGGER.log(Level.INFO, "Client received: " + request.getPayload().toString());

                counter.getAndIncrement();

                String msgToSend = "Test Message: " + counter.getAcquire();
                request = new Request(msgToSend, Instant.now());
                tcpClient.sendRequest(request);


            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Casting of request object failed!");
            } catch (EOFException e) {
                LOGGER.log(Level.INFO, "InputStream disconnected");
                try {
                    in.close();
                    out.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
            }
        };
        tcpClient = new TCPClient(HOSTNAME, PORT, clientHandler);
        tcpClient.start();
        try {
            tcpClient.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String msgToSend = "Test Message: " + counter.getAcquire();
        Request request = new Request(msgToSend, Instant.now());
        tcpClient.sendRequest(request);

        while (counter.getAcquire() < COUNT_LIMIT) {
            Thread.onSpinWait();
        }
    }

    @AfterEach
    public void tearDown() throws IOException {
        tcpClient.close();
        tcpServer.close();
    }

}
